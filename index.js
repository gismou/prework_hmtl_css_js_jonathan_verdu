function verExperiencia() {
  var element = document.getElementById('experience');
  element.classList.add("active");
  document.getElementById('studies').classList.remove("active");
  document.getElementById('languages').classList.remove("active");
  document.getElementById('tecnologies').classList.remove("active");
}

function verEstudios() {
  var element = document.getElementById('studies');
  element.classList.add("active");
  document.getElementById('experience').classList.remove("active");
  document.getElementById('languages').classList.remove("active");
  document.getElementById('tecnologies').classList.remove("active");
}

function verIdiomas() {
  var element = document.getElementById('languages');
  element.classList.add("active");
  document.getElementById('experience').classList.remove("active");
  document.getElementById('studies').classList.remove("active");
  document.getElementById('tecnologies').classList.remove("active");
}

function verTecnologias() {
  var element = document.getElementById('tecnologies');
  element.classList.add("active");
  document.getElementById('languages').classList.remove("active");
  document.getElementById('experience').classList.remove("active");
  document.getElementById('studies').classList.remove("active");
}